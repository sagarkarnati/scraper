package com.vidya.scraper.remitly;

import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import us.codecraft.xsoup.Xsoup;

public class XsoupTest {

	@Test
	public void testSelect() {

		String html = "<html><div><a href='https://github.com'>github.com</a></div>"
				+ "<table><tr><td>a</td><td>b</td></tr></table></html>";

		Document document = Jsoup.parse(html);

		String result = Xsoup.compile("//a/@href").evaluate(document).get();
		Assert.assertEquals("https://github.com", result);

		List<String> list = Xsoup.compile("//tr/td/text()").evaluate(document).list();
		Assert.assertEquals("a", list.get(0));
		Assert.assertEquals("b", list.get(1));
	}
}
