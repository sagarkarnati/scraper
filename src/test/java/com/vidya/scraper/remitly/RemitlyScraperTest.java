package com.vidya.scraper.remitly;

import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.StringUtils;

public class RemitlyScraperTest {

	private RemitlyScraper remitlyScraper;
	
	@Before
	public void setup() {
		remitlyScraper = new RemitlyScraper();
	}
	
	@Test
	public void test() {
		assertFalse(StringUtils.isEmpty(remitlyScraper.scrapeEconomyPrice()));
		assertFalse(StringUtils.isEmpty(remitlyScraper.scrapeExpressPrice()));
	}

}
