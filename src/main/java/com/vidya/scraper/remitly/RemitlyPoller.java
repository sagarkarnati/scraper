package com.vidya.scraper.remitly;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vidya.scraper.pojo.AssetEvent;

@Component
public class RemitlyPoller {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemitlyPoller.class);

	@Autowired
	private RemitlyScraper remitlyScraper;

	@Autowired
	private RemitlyDAO remitlyDAO;

	@Scheduled(cron = "0 0/5 * * * ?")
	public void poll() {

		try {
			LOGGER.info("Started REMITLY Polling");
			
			String economyPrice = remitlyScraper.scrapeEconomyPrice();
			if(economyPrice != null && economyPrice.trim().length() > 0) {
				AssetEvent economyPriceEvent = buildEvent(economyPrice, "REMITLY_ECONOMY");
				remitlyDAO.save(economyPriceEvent);
			} else {
				LOGGER.error("Not able to scrape REMITLY_ECONOMY price");
			}

			String expressPrice = remitlyScraper.scrapeExpressPrice();
			if(expressPrice != null && expressPrice.trim().length() > 0) {
				AssetEvent expressPriceEvent = buildEvent(expressPrice, "REMITLY_EXPRESS");
				remitlyDAO.save(expressPriceEvent);
			} else {
				LOGGER.error("Not able to scrape REMITLY_EXPRESS price");
			}
			LOGGER.info("REMITLY Polling is completed");
		} catch (Exception e) {
			LOGGER.error("Erro while scraping remitly", e);
		}
	}

	private AssetEvent buildEvent(String price, String source) {
		
		String priceWithOutSymbol = price.substring(1, price.length());
		
		AssetEvent assetEvent = new AssetEvent();
		assetEvent.setEventType("PRICE_CHANGE_EVENT");
		assetEvent.setAssetPriceType("SPOT");
		assetEvent.setAssetType("CURRENCY");
		assetEvent.setFromAssetName("USD");
		assetEvent.setAssetPrice(Double.parseDouble(priceWithOutSymbol));
		assetEvent.setToAssetName("INR");
		assetEvent.setSource(source);
		assetEvent.setCreatedTs(new Date());
		
		assetEvent.setAssetPrice(Double.parseDouble(priceWithOutSymbol));

		return assetEvent;
	}
}
