package com.vidya.scraper.remitly;

import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import us.codecraft.xsoup.Xsoup;

@Component
public class RemitlyScraper {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemitlyScraper.class);

	public String scrapeEconomyPrice() {

		try {
			Document document = Jsoup.parse(new URL("https://www.remitly.com/us/en/india"), 5000);

			String result = Xsoup
					.compile("//*[@id=\"c-070ed191-fa9d-44e7-9281-9dc9b06f7db3\"]/div/div/div/div/div[1]/div[2]/div/div/table/tbody/tr[1]/td[1]/text()")
					.evaluate(document).get();

			return result;

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return "";
	}

	public String scrapeExpressPrice() {

		try {
			Document document = Jsoup.parse(new URL("https://www.remitly.com/us/en/india"), 5000);

			String result = Xsoup
					.compile("//*[@id=\"c-070ed191-fa9d-44e7-9281-9dc9b06f7db3\"]/div/div/div/div/div[1]/div[1]/div/div/table/tbody/tr[1]/td[1]/text()")
					.evaluate(document).get();

			return result;

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return "";
	}
}
