package com.vidya.scraper.remitly;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RemitlyController {
	
	@Autowired
	private RemitlyScraper remitlyScraper;
	
	@RequestMapping(method = RequestMethod.GET, path="/remitly/india/price/economy/current")
	public String fetchCurrentEconomyPrice() {
		
		return remitlyScraper.scrapeEconomyPrice();
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/remitly/india/price/express/current")
	public String fetchCurrentExpressPrice() {
		return remitlyScraper.scrapeExpressPrice();
	}
}
