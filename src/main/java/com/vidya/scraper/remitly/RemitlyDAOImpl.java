package com.vidya.scraper.remitly;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.vidya.scraper.pojo.AssetEvent;

@Repository
public class RemitlyDAOImpl implements RemitlyDAO {
	
	private static final String INSERT_ASSET_EVENT = "INSERT INTO ASSET_EVENT (EVENT_TYPE, ASSET_PRICE_TYPE, ASSET_TYPE,"
			+ "FROM_ASSET_NAME,ASSET_PRICE,TO_ASSET_NAME,SOURCE,CREATED_TS) VALUES (?,?,?,?,?,?,?,?)";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void save(AssetEvent assetEvent) {
		
		jdbcTemplate.update(INSERT_ASSET_EVENT, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement preparedStatement) throws SQLException {
				
				preparedStatement.setString(1, assetEvent.getEventType());
				preparedStatement.setString(2, assetEvent.getAssetPriceType());
				preparedStatement.setString(3, assetEvent.getAssetType());
				preparedStatement.setString(4, assetEvent.getFromAssetName());
				preparedStatement.setDouble(5, assetEvent.getAssetPrice());
				preparedStatement.setString(6, assetEvent.getToAssetName());
				preparedStatement.setString(7, assetEvent.getSource());
				preparedStatement.setTimestamp(8, new java.sql.Timestamp(assetEvent.getCreatedTs().getTime()));
			}
		});
	}
}
