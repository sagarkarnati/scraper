package com.vidya.scraper.pojo;

import java.io.Serializable;
import java.util.Date;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class AssetEvent implements Serializable {

	private long id;
	private String eventType;
	private String assetPriceType;
	private String assetType;
	private String fromAssetName;
	private Double assetPrice;
	private String toAssetName;
	private String source;
	private Date createdTs;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getAssetPriceType() {
		return assetPriceType;
	}

	public void setAssetPriceType(String assetPriceType) {
		this.assetPriceType = assetPriceType;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getFromAssetName() {
		return fromAssetName;
	}

	public void setFromAssetName(String fromAssetName) {
		this.fromAssetName = fromAssetName;
	}

	public Double getAssetPrice() {
		return assetPrice;
	}

	public void setAssetPrice(Double assetPrice) {
		this.assetPrice = assetPrice;
	}

	public String getToAssetName() {
		return toAssetName;
	}

	public void setToAssetName(String toAssetName) {
		this.toAssetName = toAssetName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getCreatedTs() {
		return createdTs;
	}

	public void setCreatedTs(Date createdTs) {
		this.createdTs = createdTs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assetPrice == null) ? 0 : assetPrice.hashCode());
		result = prime * result + ((assetPriceType == null) ? 0 : assetPriceType.hashCode());
		result = prime * result + ((assetType == null) ? 0 : assetType.hashCode());
		result = prime * result + ((createdTs == null) ? 0 : createdTs.hashCode());
		result = prime * result + ((eventType == null) ? 0 : eventType.hashCode());
		result = prime * result + ((fromAssetName == null) ? 0 : fromAssetName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((toAssetName == null) ? 0 : toAssetName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssetEvent other = (AssetEvent) obj;
		if (assetPrice == null) {
			if (other.assetPrice != null)
				return false;
		} else if (!assetPrice.equals(other.assetPrice))
			return false;
		if (assetPriceType == null) {
			if (other.assetPriceType != null)
				return false;
		} else if (!assetPriceType.equals(other.assetPriceType))
			return false;
		if (assetType == null) {
			if (other.assetType != null)
				return false;
		} else if (!assetType.equals(other.assetType))
			return false;
		if (createdTs == null) {
			if (other.createdTs != null)
				return false;
		} else if (!createdTs.equals(other.createdTs))
			return false;
		if (eventType == null) {
			if (other.eventType != null)
				return false;
		} else if (!eventType.equals(other.eventType))
			return false;
		if (fromAssetName == null) {
			if (other.fromAssetName != null)
				return false;
		} else if (!fromAssetName.equals(other.fromAssetName))
			return false;
		if (id != other.id)
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (toAssetName == null) {
			if (other.toAssetName != null)
				return false;
		} else if (!toAssetName.equals(other.toAssetName))
			return false;
		return true;
	}

}
